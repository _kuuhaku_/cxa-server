import sys

class route:
   def __init__(self, name, start_x, start_y):
      self.name = name
      self.start_x = start_x
      self.start_y = start_y
      self.path = []
   def addpath(self, direction, steps):
      self.path.append((direction, steps))
class map:
   def __init__(self, name, image, resolution, threshold):
      self.name = name
      self.image = image
      self.resolution = resolution
      self.threshold = threshold
      self.mapdata = []
      self.route = []
      self.agent = []
   def updatemap(self, mapdata):
      for i in mapdata:
         self.mapdata.append([])
         for j in i:
            self.mapdata[-1].append(j)
   def addroute(self, route):
      self.route.append(route)
   def addagent(self, a):
      self.agent.append(a)
class agent:
   def __init__(self, serial, transp):
      self.serial = serial
      self.transp = transp
maplist = []
def readmapdata():
   f = open("map.txt", "r")
   readingmap = False
   lines = f.read().split('\n')
   for l in lines:
      if 'MapName' in l:
         name = l[l.find('=') + 1:l.rfind('------')]
         # print "Name:", name
      if 'Image' in l:
         image = l[l.find('=') + 1:]
         # print "Image:", image
      if 'Resolution' in l:
         resolution = int(l[l.find('=') + 1:])
         # print "Resolution:", resolution
      if 'Threshold' in l:
         threshold = int(l[l.find('=') + 1:])
         # print "Threshold:", threshold
      if 'MapData' in l:
         readingmap = True
         mapdata = []
         continue
      if l.strip() == "":
         if readingmap:
            maplist.append(map(name, image, resolution, threshold))
            maplist[-1].updatemap(mapdata)
            readingmap = False
      if readingmap:
         t = l.split()
         mapdata.append([]);
         for i in t:
            mapdata[-1].append(int(i))
   f.close()

readmapdata();

def writemapdata():
   f = open("map.txt", "w")
   for m in maplist:
      f.write("------MapName={}------\n".format(m.name))
      f.write("Image={}\n".format(m.image))
      f.write("Resolution={}\n".format(m.resolution))
      f.write("Threshold={}\n".format(m.threshold))
      f.write("MapData=\n")
      for i in m.mapdata:
         for j in i:
            f.write(str(j) + " ")
         f.write("\n")
      f.write("\n")

def readroutedata():
   f = open("route.txt", "r")
   readingpath = False
   lines = f.read().split('\n')
   for l in lines:
      if 'MapName' in l:
         name = l[l.find('=') + 1:l.rfind('------')]
      if 'Route' in l:
         routename = l[l.find('=') + 1:]
      if 'Start' in l:
         tmp = l[l.find('=') + 1:]
         tmp = tmp.split()
         start_x = int(tmp[0])
         start_y = int(tmp[1])
      if 'PathData' in l:
         readingpath = True
         pathdata = []
         continue
      if l.strip() == "":
         if readingpath:
            for m in maplist:
               if m.name == name:
                  m.addroute(route(routename, start_x, start_y))
                  for i in pathdata:
                     m.route[-1].addpath(i[0], i[1])
                  break
      if readingpath:
         tmp = l.strip().split()
         pathdata.append(tmp)

readroutedata();

# sys.exit()

import socket
s = socket.socket()
host = "192.168.1.101"
port = 1234
s.bind((host, port))
s.listen(5)
print 'Host address: {}, Port: {}'.format(host, port)                 
print 'Waiting for connection...'
while True:
   c, addr = s.accept()     
   print 'Accepted connection from', addr
   while True:
      t = c.recv(1024).strip()
      if t == 'exit':
         c.close()
         break
      elif t == 'fetchmaplist':
         tmp = ""
         for i in maplist:
            tmp += i.name + '|'
         c.send(tmp)
      elif 'fetchmapinfo' in t:
         mapname = t[t.find(":") + 1:].strip()
         for m in maplist:
            if m.name == mapname:
               c.send(m.image)
               c.recv(1024)
               c.send(str(m.resolution))
               c.recv(1024)
               c.send(str(m.threshold))
               c.recv(1024)
               c.send("END")
               break
      elif 'fetchmapdata' in t:
         mapname = t[t.find(":") + 1:].strip()
         for m in maplist:
            if m.name == mapname:
               for i in m.mapdata:
                  tmp = ""
                  for j in i:
                     tmp += str(j) + ' '
                  c.send(tmp)
                  c.recv(1024)
               c.send('END')
               break
      elif 'uploadmapdata' in t:
         mapname = t[t.find(":") + 1:].strip()
         c.send("Done")
         t = c.recv(1024)
         image = t.strip()
         c.send("Done")
         t = c.recv(1024)
         resolution = int(t.strip())
         c.send("Done")
         t = c.recv(1024)
         threshold = int(t.strip())
         c.send("Done")
         for m in maplist:
            if m.name == mapname:
               m = map(mapname, image, resolution, threshold)
               break
         maplist.append(map(mapname, image, resolution, threshold))
         m = maplist[-1]
         mapdata = []
         while(True):
            t = c.recv(1024).strip()
            if t == "END":
               break
            c.send("Done")
            tmp = t.split()
            mapdata.append([])
            for i in tmp:
               mapdata[-1].append(int(i))
         m.updatemap(mapdata)
         writemapdata()
         while(True):
            t = c.recv(1024).strip()
            if t == "END":
               break
            c.send("Done")
            m.addagent(agent(len(m.agent) + 1, t))
            print t
      else:
         print t
         c.send("received.")

